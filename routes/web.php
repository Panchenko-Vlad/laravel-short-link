<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/link', 'LinkController');
Route::resource('/redirect', 'RedirectController');

Route::get('/', 'LinkController@create')->name('home');
Route::get('/r/{slug}', 'RedirectController@create')->name('redirect');

Route::get('/redirects', 'RedirectController@index')->name('redirects');
Route::get('/stats', 'RedirectController@stats')->name('stats');

Route::get('/get-data', 'RedirectController@getData');
Route::get('/get-stats', 'RedirectController@getStats');
