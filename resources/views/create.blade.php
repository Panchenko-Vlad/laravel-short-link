@extends('layouts.app')

@section('content')
    <div class="create-page">
        <div class="insert-link">
            @include('common.alerts')

            <div class="jumbotron">
                <div class="col-lg-4 offset-lg-4
                            col-md-6 offset-md-3">
                    <h1 class="text-center">Insert link</h1>

                    <hr class="my-4">

                    <form action="{{ route('link.store') }}" method="post">
                        {{ csrf_field() }}

                        @include('partials.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
