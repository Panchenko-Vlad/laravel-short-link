@extends('layouts.app')

@section('content')
    <div class="data-table">
        <h4>List of redirects</h4>

        <hr>

        <grid-view action="/get-data" :fields="[
            {
                key: 'id',
                label: '№',
                sortable: true,
                thStyle: 'min-width: 50px; width: 50px'
            },
            {
                key: 'link.url',
                label: 'URL',
                sortable: true
            },
            {
                key: 'link.slug',
                label: 'Slug',
                sortable: true
            },
            {
                key: 'language',
                sortable: true
            },
            {
                key: 'ip',
                label: 'IP',
                sortable: true,
            },
            {
                key: 'user_agent',
                sortable: true
            },
            {
                key: 'headers',
                sortable: false,
                formatter: function(value) {
                    let headers = JSON.parse(value);

                    let list = [];
                    Object.entries(headers).map(([key, value]) => {
                        // Add spaces
                        value = value.toString().split(';').join('; ');
                        value = value.toString().split(',').join(', ');

                        // Add header
                        list.push(key + ': ' + value);
                    });

                    return list.join(' | ');
                }
            },
        ]">
        </grid-view>
    </div>
@endsection
