<div class="form-group">
    <label for="url">Your URL</label>
    <input type="url" name="url" class="form-control" id="url">
</div>

<div class="form-group">
    <label for="slug">Short name</label>
    <input type="text" name="slug" class="form-control" id="slug">
</div>

<button type="submit" class="btn btn-primary">Submit</button>
