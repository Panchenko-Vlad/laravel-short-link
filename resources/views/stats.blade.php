@extends('layouts.app')

@section('content')
    <div class="data-table">
        <h4>Statistics of redirects</h4>

        <hr>

        <grid-view action="/get-stats" :fields="[
            {
                key: 'id',
                label: '№',
                sortable: true,
                thStyle: 'min-width: 50px; width: 50px'
            },
            {
                key: 'link.url',
                label: 'URL',
                sortable: true
            },
            {
                key: 'link.slug',
                label: 'Slug',
                sortable: true
            },
            {
                key: 'redirects_count',
                label: 'Redirects',
                sortable: true
            }
        ]">
        </grid-view>
    </div>
@endsection
