<?php

namespace App\Helpers;

use App\Link;

class LinkDisplay
{
    public static function url(Link $link)
    {
        return route('redirect', ['slug' => $link->slug]);
    }
}
