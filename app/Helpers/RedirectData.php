<?php

namespace App\Helpers;

use App\Redirect;
use Illuminate\Support\Facades\DB;

class RedirectData
{
    public static function getAll()
    {
        return Redirect::with('link')->orderBy('id', 'desc')->get();
    }

    public static function getStats()
    {
        return Redirect::with('link:id,url,slug')
            ->select(DB::Raw('id, link_id, COUNT(link_id) as redirects_count'))
            ->groupBy('link_id')
            ->get();
    }
}
