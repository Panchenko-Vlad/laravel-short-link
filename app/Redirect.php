<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $fillable = ['headers', 'user_agent', 'ip', 'language'];

    public function link()
    {
        return $this->belongsTo(Link::class);
    }

    public function setHeadersAttribute($value)
    {
        unset($value['user-agent']);
        unset($value['cookie']);

        $this->attributes['headers'] = json_encode($value);
    }

    public function setLanguageAttribute($acceptLanguage)
    {
        $this->attributes['language'] = explode(',', $acceptLanguage)[0];
    }
}
