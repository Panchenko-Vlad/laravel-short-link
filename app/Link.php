<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Link extends Model
{
    protected $fillable = ['url', 'slug'];

    public function redirects()
    {
        return $this->hasMany(Redirect::class);
    }

    public function setSlugAttribute($value)
    {
        $slug = $value . '-' . Str::random(6);
        $this->attributes['slug'] = Str::slug($slug, '-');
    }
}
