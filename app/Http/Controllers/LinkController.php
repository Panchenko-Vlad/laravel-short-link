<?php

namespace App\Http\Controllers;

use App\Helpers\LinkDisplay;
use App\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'url' => 'required|url',
            'slug' => 'required|unique:links'
        ]);

        $link = Link::create($request->all());

        return redirect()->back()->with('message', 'Your short link: ' . LinkDisplay::url($link));
    }
}
