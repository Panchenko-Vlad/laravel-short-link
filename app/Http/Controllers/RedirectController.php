<?php

namespace App\Http\Controllers;

use App\Helpers\RedirectData;
use App\Link;
use Illuminate\Http\Request;

class RedirectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response|array
     */
    public function index(Request $request)
    {
        return view('redirects');
    }

    public function getData()
    {
        return RedirectData::getAll()->toArray();
    }

    /**
     * Display a listing of the statistics.
     *
     * @return \Illuminate\Http\Response|array
     */
    public function stats(Request $request)
    {
        return view('stats');
    }

    public function getStats()
    {
        return RedirectData::getStats()->toArray();
    }

    /**
     * Redirect user
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $slug)
    {
        $link = Link::where('slug', $slug)->firstOrFail();

        $link->redirects()->create([
            'headers' => $request->headers->all(),
            'user_agent' => $request->userAgent(),
            'ip' => $request->getClientIp(),
            'language' => $request->server('HTTP_ACCEPT_LANGUAGE')
        ]);

        return redirect()->to($link->url);
    }
}
